<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    // public function __construct()
    // {
    //     return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::all();
        return response()->json([
            'success'   => true,
            'message'   => 'Data daftar product berhasil ditampilkan',
            'data'   => $product,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $isAdmin = Auth::user()->role;
        if ($isAdmin) {
            if ($isAdmin->description != 'admin') {
                return response()->json([
                    'success'   => false,
                    'message'   => 'Not Allowed',
                ], 403);
            }
        } else {
            return response()->json([
                'success'   => false,
                'message'   => 'Hubungi Admin',
            ], 401);
        }

        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name'          => 'required|unique:products,name',
            'price'         => 'required|numeric',
            'quantity'      => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $product = Product::create([
            'name'              => $request->name,
            'price'             => $request->price,
            'quantity'          => $request->quantity,
        ]);

        if ($product) {
            return response()->json([
                'success'   => true,
                'message'   => 'Data product berhasil dibuat',
                'data'      => $product,
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data product gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        if ($product) {
            return response()->json([
                'success'       => true,
                'message'       => 'Data product berhasil ditampilkan',
                'data'          => $product
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $isAdmin = Auth::user()->role;
        if ($isAdmin) {
            if ($isAdmin->description != 'admin') {
                return response()->json([
                    'success'   => false,
                    'message'   => 'Not Allowed',
                ], 403);
            }
        } else {
            return response()->json([
                'success'   => false,
                'message'   => 'Hubungi Admin',
            ], 401);
        }

        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'name'          => 'required|unique:products,name,' . $id . ',id',
            'price'         => 'required|numeric',
            'quantity'      => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $product = Product::find($id);

        if ($product) {
            $product->update([
                'name'          => $request->name,
                'price'         => $request->price,
                'quantity'      => $request->quantity,
            ]);

            return response()->json([
                'success'       => true,
                'message'       => 'Data role dengan id : ' . $id . ' berhasil diubah',
                'data'          => $product
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $isAdmin = Auth::user()->role;

        if ($isAdmin) {
            if ($isAdmin->description != 'admin') {
                return response()->json([
                    'success'   => false,
                    'message'   => 'Not Allowed',
                ], 403);
            }
        } else {
            return response()->json([
                'success'   => false,
                'message'   => 'Hubungi Admin',
            ], 401);
        }

        $product = Product::find($id);

        if ($product) {
            $product->delete();
            return response()->json([
                'success'       => true,
                'message'       => 'Data product berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }
}
