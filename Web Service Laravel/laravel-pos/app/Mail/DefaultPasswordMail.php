<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DefaultPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    public $def_pass;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($def_pass)
    {
        $this->def_pass = $def_pass;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.pass.default_password_mail')
            ->subject('Default Password - Laravel POS');
    }
}
