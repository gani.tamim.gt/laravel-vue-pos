<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;

class MeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {

        $auth = Auth::user();
        // $auth = User::where('id', 'adfafaafafd-afadff-453434aaa-dadad')->first();
        $me = $auth;
        $me['profile'] = $auth->profile;
        return response()->json([
            'data'          => $me,
        ], 200);
    }
}
