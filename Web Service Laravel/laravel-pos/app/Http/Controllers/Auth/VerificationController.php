<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Events\UserVerified;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'otp'         => 'required|exists:otp_codes,otp',
            'email'       => 'required|exists:users,email',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user_email = User::where('email', $request->email)->first();
        if ($user_email->email_verified_at != null) {
            return response()->json([
                'success'       => false,
                'message'       => 'Email sudah terverifikasi'
            ], 400);
        }
        $otp_code = OtpCode::where('otp', $request->otp)->first();

        $user = User::find($otp_code->user_id);
        if ($request->email != $user->email) {
            return response()->json([
                'success'       => false,
                'message'       => 'OTP tidak sesuai'
            ], 400);
        }

        $now = Carbon::now();
        if ($now > $otp_code->valid_until) {
            return response()->json([
                'success'       => false,
                'message'       => 'OTP tidak berlaku lagi'
            ], 400);
        }

        $def_pass = substr(str_shuffle("12345677890abcdefghijklmnopqrstuvwxyz"), 0, 6);

        $user->update([
            'email_verified_at'     => $now,
            'password'              => Hash::make($def_pass)
        ]);

        //memanggil event UserVerified
        $email = $request->email;
        event(new UserVerified($def_pass, $email));

        $otp_code->delete();

        return response()->json([
            'success'       => true,
            'message'       => 'User berhasil diverifikasi, password telah dikirim ke email Anda',
            'data'          => $user
        ], 200);
    }
}
