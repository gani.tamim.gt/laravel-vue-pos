<?php

use App\Http\Middleware\checkAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//example of apiResource
// Route::get('/post', 'PostController@index');
// Route::post('/post', 'PostController@store')->middleware('auth:api');
// Route::get('/post/{id}', 'PostController@show');
// Route::put('/post/{id}', 'PostController@update');
// Route::delete('/post/{id}', 'PostController@destroy');

Route::apiResource('role', 'RoleController')->middleware(['auth:api', 'checkAdmin']);
Route::apiResource('profile', 'ProfileController')->middleware('auth:api');
Route::apiResource('product', 'ProductController')->middleware('auth:api');
Route::apiResource('order', 'OrderController')->middleware('auth:api');
Route::post('auth/logout', 'Auth\LogoutController')->middleware('auth:api');
Route::post('auth/me', 'Auth\MeController')->middleware('auth:api');

Route::group([
    'prefix'    => 'auth',
    'namespace' => 'Auth'
], function () {
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate_otp_code');
    Route::post('verification', 'verificationController')->name('auth.verification');
    Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login', 'LoginController')->name('auth.login');
});
