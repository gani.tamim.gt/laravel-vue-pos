<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    // public function __construct()
    // {
    //     return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::all()->where('description', '!=', 'admin');
        return response()->json([
            'success'   => true,
            'message'   => 'Data daftar role berhasil ditampilkan',
            'data'   => $role,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'description'    => 'required|unique:roles,description',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $role = Role::create([
            'description'         => $request->description,
        ]);

        if ($role) {
            return response()->json([
                'success'   => true,
                'message'   => 'Data role berhasil dibuat',
                'data'   => $role,
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data role gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = role::find($id);

        if ($role->description === 'admin') {
            return response()->json([
                'success'       => false,
                'message'       => 'Not Allowed',
            ], 403);
        }

        if ($role) {
            return response()->json([
                'success'       => true,
                'message'       => 'Data role berhasil ditampilkan',
                'data'          => $role
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'description'         => 'required|unique:roles,description',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $role = role::find($id);

        if ($role->description === 'admin') {
            return response()->json([
                'success'       => false,
                'message'       => 'Not Allowed',
            ], 403);
        }

        if ($role) {
            $role->update([
                'description'         => $request->description,
            ]);

            return response()->json([
                'success'       => true,
                'message'       => 'Data role dengan id : ' . $id . ' berhasil diubah',
                'data'          => $role
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $role = role::find($id);

        if ($role->description === 'admin') {
            return response()->json([
                'success'       => false,
                'message'       => 'Not Allowed',
            ], 403);
        }

        if ($role) {
            $role->delete();
            return response()->json([
                'success'       => true,
                'message'       => 'Data role berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }
}
