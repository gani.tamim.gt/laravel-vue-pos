<?php

namespace App\Listeners;

use App\Mail\DefaultPasswordMail;
use App\Events\UserVerified;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailDefaultPassword implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserVerified  $event
     * @return void
     */
    public function handle(UserVerified $event)
    {
        Mail::to($event->email)->send(new DefaultPasswordMail($event->def_pass));
    }
}
