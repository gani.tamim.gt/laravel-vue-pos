<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //get admin value of user from database
        if (Auth::user()->role_id === null) {
            return response()->json([
                'success'   => false,
                'message'   => 'User not allowed !',
            ], 401);
        }
        if (Auth::user()->role->description != 'admin') {
            return response()->json([
                'success'   => false,
                'message'   => 'User not allowed !',
            ], 401);
        }

        return $next($request);
    }
}
