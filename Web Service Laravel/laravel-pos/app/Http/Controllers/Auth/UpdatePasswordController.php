<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'email'         => 'required',
            'password'      => 'required|min:6',
            'new_password'  => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'success'       => false,
                'message'       => 'Password salah'
            ], 401);
        }

        $user = User::where('email', $request->email)->first();

        $user->update([
            'password'      => Hash::make($request->new_password)
        ]);

        return response()->json([
            'success'       => true,
            'message'       => 'Password berhasil diubah',
            'data'          => $user
        ], 200);
    }
}
