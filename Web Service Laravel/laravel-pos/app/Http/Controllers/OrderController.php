<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    // public function __construct()
    // {
    //     return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_id = $request->user_id;

        $order = Order::where('user_id', $user_id)->get();
        $name = Order::where('user_id', $user_id)->first()->user->profile->name;

        return response()->json([
            'success'   => true,
            'message'   => 'Data daftar order berhasil ditampilkan',
            'data'      => $order,
            'name'      => $name,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $isKasir = Auth::user()->role;
        if ($isKasir) {
            if ($isKasir->description != 'kasir') {
                return response()->json([
                    'success'   => false,
                    'message'   => 'Silahkan hubungi kasir untuk pembelian',
                ], 403);
            }
        } else {
            return response()->json([
                'success'   => false,
                'message'   => 'Hubungi Admin',
            ], 401);
        }

        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'quantity'          => 'required|numeric',
            'user_id'           => 'required|exists:users,id',
            'product_id'        => 'required|exists:products,id',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $product = Product::where('id', $request->product_id)->first();
        $stok = $product->value('quantity');

        if (($stok - $request->quantity) < 0) {
            return response()->json([
                'success'   => false,
                'message'   => 'Stok product tidak cukup',
            ], 401);
        }

        $order = Order::create([
            'quantity'              => $request->quantity,
            'user_id'               => $request->user_id,
            'product_id'            => $request->product_id,
        ]);

        if ($order) {
            $product->update([
                'quantity'         => ($stok - $request->quantity),
            ]);
            return response()->json([
                'success'   => true,
                'message'   => 'Data order berhasil dibuat',
                'data'      => $order,
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data order gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);

        if ($order) {
            return response()->json([
                'success'       => true,
                'message'       => 'Data order berhasil ditampilkan',
                'data'          => $order
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $isAdmin = Auth::user()->role;
        if ($isAdmin) {
            if ($isAdmin->description != 'admin') {
                return response()->json([
                    'success'   => false,
                    'message'   => 'Pembelian yang sudah dibayar tidak bisa diubah',
                ], 403);
            }
        } else {
            return response()->json([
                'success'   => false,
                'message'   => 'Hubungi Admin',
            ], 401);
        }

        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'quantity'          => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $order = Order::find($id);

        if ($order) {
            $stok = $order->product->quantity;
            $jml_beli_lama = $order->quantity;

            if (($stok + $jml_beli_lama - $request->quantity) < 0) {
                return response()->json([
                    'success'   => false,
                    'message'   => 'Stok product tidak cukup',
                ], 401);
            }

            $order->update([
                'quantity'         => $request->quantity,
            ]);
            $order->product->update([
                'quantity'         => ($stok + $jml_beli_lama - $request->quantity),
            ]);

            return response()->json([
                'success'       => true,
                'message'       => 'Data order dengan id : ' . $id . ' berhasil diubah',
                'data'          => $order
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $isAdmin = Auth::user()->role;
        if ($isAdmin) {
            if ($isAdmin->description != 'admin') {
                return response()->json([
                    'success'   => false,
                    'message'   => 'Pembelian yang sudah dibayar tidak bisa dihapus',
                ], 403);
            }
        } else {
            return response()->json([
                'success'   => false,
                'message'   => 'Hubungi Admin',
            ], 401);
        }

        $order = Order::find($id);

        if ($order) {
            $stok = $order->product->quantity;
            $jml_beli_lama = $order->quantity;

            $order->product->update([
                'quantity'         => ($stok + $jml_beli_lama),
            ]);

            $order->delete();
            return response()->json([
                'success'       => true,
                'message'       => 'Data order berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }
}
