<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    // public function __construct()
    // {
    //     return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role === null) {
            return response()->json([
                'success'   => false,
                'message'   => 'Harap hubungi admin terlebih dahulu',
            ], 401);
        }
        if (Auth::user()->role->description === 'admin') {
            $profile = Profile::all();
        } else {
            $profile = Profile::where('user_id', Auth::user()->id)->first();
        }

        return response()->json([
            'success'   => true,
            'message'   => 'Data daftar profile berhasil ditampilkan',
            'data'      => $profile,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->role === null) {
            return response()->json([
                'success'   => false,
                'message'   => 'Harap hubungi admin terlebih dahulu',
            ], 401);
        }

        $user_id = $request->user_id;
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name'          => 'required',
            'nik'           => 'required|unique:profiles,nik|numeric',
            'address'       => 'required',
            'user_id'       => 'required|unique:profiles,user_id',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        if (Auth::user()->id != $user_id) {
            return response()->json([
                'success'   => false,
                'message'   => 'Tidak bisa membuat profil untuk orang lain',
            ], 403);
        }

        $profile = Profile::create([
            'name'              => $request->name,
            'nik'               => $request->nik,
            'address'           => $request->address,
            'user_id'           => $user_id,
        ]);

        if ($profile) {
            return response()->json([
                'success'   => true,
                'message'   => 'Data profile berhasil dibuat',
                'data'   => $profile,
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data profile gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::find($id);

        if ($profile) {
            return response()->json([
                'success'       => true,
                'message'       => 'Data profile berhasil ditampilkan',
                'data'          => $profile
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'name'          => 'required',
            'nik'           => 'required|unique:profiles,nik,' . $id . ',id|numeric',
            'address'       => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $profile = Profile::find($id);

        if ($profile) {

            $user_id = $profile['user_id'];

            if ($user_id != Auth::user()->id) {
                return response()->json([
                    'success'       => false,
                    'message'       => 'Not Allowed',
                ], 401);
            }

            $profile->update([
                'name'         => $request->name,
                'nik'          => $request->nik,
                'address'      => $request->address,
            ]);

            return response()->json([
                'success'       => true,
                'message'       => 'Data profile dengan id : ' . $id . ' berhasil diubah',
                'data'          => $profile
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $profile = Profile::find($id);

        if ($profile) {

            $user_id = $profile['user_id'];
            // return response()->json([
            //     'success'       => false,
            //     'message'       => Auth::user()->id,
            // ], 401);
            if ($user_id != Auth::user()->id) {
                return response()->json([
                    'success'       => false,
                    'message'       => 'Not Allowed',
                ], 401);
            }
            $profile->delete();
            return response()->json([
                'success'       => true,
                'message'       => 'Data profile berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }
}
