<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\RegisterOtpMail;
use App\Events\UserRegistered;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'email'         => 'required|unique:users,email|email',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // $data = OtpCode::all()->first();
        // return response()->json([
        //     'data'           => $data,

        // ]);

        $user = User::create($allRequest);

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp'           => $random,
            'valid_until'   => $now->addMinutes(5),
            'user_id'       => $user->id
        ]);


        //kirim otp ke email user
        //memanggil event UserRegistered
        event(new UserRegistered($otp_code));
        // // mail ke pemilik post
        // Mail::to($otp_code->user->email)->send(new RegisterOtpMail($otp_code));

        return response()->json([
            'success'           => true,
            'message'           => 'Data user berhasil dibuat',
            'data'              => [
                'user'              => $user,
                'otp_code'          => $otp_code
            ]
        ]);
    }
}
